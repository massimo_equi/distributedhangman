package server;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPRange {
	private int first;
	private boolean[] IPfree;
	
	public IPRange(String first, int dim){
		try {
			this.first = pack(InetAddress.getByName(first).getAddress());
		} catch (UnknownHostException e) {
			System.err.println("IPRange: Problems in costructor");
			System.exit(1);
		}
		this.IPfree = new boolean[dim];
		for(int i = 0; i < this.IPfree.length; i++)
			this.IPfree[i] = true;
	}
	
	public synchronized String getNextFreeIP(){
		int i = 0, free = -1;
		while(i < this.IPfree.length && free == -1){
			if(this.IPfree[i]){
				free = i;
				this.IPfree[i] = false;
			}
			i++;					
		}
		
		String res = null;
				
		if(free != -1)
			try {
				res = InetAddress.getByAddress(unpack(this.first + free)).getHostAddress();
			} catch (UnknownHostException e) {
				System.err.println("IPRange: Problems parsing IP");
				System.exit(1);
			}
		
		return res;
	}
	
	public synchronized void freeIP(String IP){
		int i = 0, free = -1;
		int intIP = 0;
		try {
			intIP = pack(InetAddress.getByName(IP).getAddress());
		} catch (UnknownHostException e) {
			System.err.println("IPRange: Problems parsing IP");
			System.exit(1);
		}
		while(i < this.IPfree.length && free == -1){
			if(this.first + i == intIP){
				free = i;
				this.IPfree[i] = true;
			}
			i++;					
		}
	}
	
	public boolean isFree(){
		int i = 0;
		boolean free = false;
		while(i < this.IPfree.length && !free){
			if(this.IPfree[i]){
				free = true;
				this.IPfree[i] = false;
			}
			i++;					
		}
		
		return free;
	}
	private  int pack(byte[] bytes) {
	  int val = 0;
	  for (int i = 0; i < bytes.length; i++) {
	    val <<= 8;
	    val |= bytes[i] & 0xff;
	  }
	  return val;
	}
		
	private  byte[] unpack(int bytes) {
	  return new byte[] {
	    (byte)((bytes >>> 24) & 0xff),
	    (byte)((bytes >>> 16) & 0xff),
	    (byte)((bytes >>>  8) & 0xff),
	    (byte)((bytes       ) & 0xff)
	  };
	}
}

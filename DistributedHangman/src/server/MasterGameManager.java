package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.Vector;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import client.IClientStub;

public class MasterGameManager extends Thread{
	private static final long TIMEOUT = 600000;//10 minuti
	private static final String multicastPORT = "40000";
		
	private Vector<Player> players;
	private Vector<Game> games;
	private Socket socket;
	private String multicastIP;
	private IPRange multicastIPs;
	
	public MasterGameManager(Socket socket, Vector<Player> players, Vector<Game> games,
			IPRange multicastIPs){
		this.socket = socket;
		this.games = games;
		this.players = players;
		this.multicastIPs = multicastIPs;
		this.multicastIP = multicastIPs.getNextFreeIP();
		
	}
	
	@SuppressWarnings("unchecked")
	public void run(){
		BufferedReader input = null;
		PrintWriter output = null;
		JSONObject j = new JSONObject();
		String nickname, password;
		int guessersNUM;
		Game g = null;

		//Apertura degli stream per la comunicazione TCP
		try {
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			System.err.println("MasterGameManager: Problems with socket's streams");
			System.exit(1);
		}
		
		//Lettura dal socket TCP dei dati necessari per inizializzare la partita
		try {
			j = (JSONObject) new JSONParser().parse(input.readLine());
		} catch (ParseException e1) {
			System.err.println("MasterGameManager: Problems parsing socket input");
			System.exit(1);
		} catch (IOException e1) {
			System.err.println("MasterGameManager: Problems reading scoket input");
			System.exit(1);
		}		
		
		nickname = (String) j.get("nickname");
		password = (String) j.get("password");
		guessersNUM = Integer.parseInt((String) j.get("guessersNUM"));
		
		//Creazione della nuova partita
		g = new Game(nickname, guessersNUM, multicastIP);
		
		g.setEcryptKey(new Integer(new Random(System.currentTimeMillis()).nextInt()).toString());
		
		if(this.autenticate(nickname, password)){
			this.players.get(this.players.indexOf(new Player(nickname))).setFree(false);
			
			long elapsedTime = 0, before, after;
			synchronized(g.getMasterMonitor()){
				//Si aggiunge la nuova partita alla lista delle partite disponibili
				this.games.add(g);
				//Si rende visibile la nuova lista delle partite disponibili
				this.refreshGameList();
				
				//Si lancia un nuovo thread per monitorare l'arrivo di un'eventuale richiesta di
				//abbandono
				MasterMonitorThread th = new MasterMonitorThread(g, input);
				th.start();
				while(!g.getAborted() && g.getGuessersNUM() < guessersNUM && TIMEOUT > elapsedTime){
					before = System.currentTimeMillis();
					try {
						g.getMasterMonitor().wait(TIMEOUT - elapsedTime);
					}
					catch (InterruptedException e){
						System.err.println("MasterGameManager: Problems waiting for guessers");
						System.exit(1);
					}
					after = System.currentTimeMillis();
					elapsedTime += after - before;
				}
				
				if(g.getAborted() || TIMEOUT <= elapsedTime){
					g.setAborted(true);//Nel caso di timeout il flag non è ancora stato settato
					g.getMasterMonitor().notifyAll();
				}
				this.games.remove(g);
				this.refreshGameList();	
			}

			if(!g.getAborted()){
				j.put("multicastIP", this.multicastIP);
				j.put("multicastPORT", multicastPORT);
				j.put("aborted", "false");		
				j.put("encryptKey", g.getEncryptKey());
				
				System.out.println("A new game started.");
			}
			else{
				j.put("aborted", "true");				
			}			
		}
		else{
			j.put("aborted", "true");			
			System.out.println("Connection rejected");			
		}
		output.println(j.toJSONString());
		
		this.multicastIPs.freeIP(this.multicastIP);
		
		try{
			this.socket.close();
		}
		catch (IOException e){
			System.err.println("MasterGameManager: Problems closing the socket");
		}
	}
	
	private boolean autenticate(String nickname, String password){
		boolean autenticated = false;
		Player p;
		int index = this.players.indexOf(new Player(nickname));
		
		if(index > -1){
			p = this.players.get(index);
			if(HangmanRegistry.pwdEncryptor.checkPassword(password, p.getPassword())
					&& p.getLogged()){
				autenticated = true;
			}
		}
		
		return autenticated;
	}
	
	private void refreshGameList(){
		String gameList = "";
		for(Game game : games){
			gameList += game + "\n";
		}
		
		try{	
			IClientStub cb = null;
			for(Player p1 : this.players){
				if((cb = p1.getCallback()) != null && p1.getFree()){
					cb.refreshGamesList(gameList);
				}
			}
		} catch (RemoteException e){
			System.err.println(
					"MasterGameManager: Problems notifing game list changes to other players");
			System.exit(1);
		}
	}

}

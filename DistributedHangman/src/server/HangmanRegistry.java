package server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;

import org.jasypt.util.password.BasicPasswordEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class HangmanRegistry {
	private static final String configFile = "RegistryConfiguration.json";
	public static final BasicPasswordEncryptor pwdEncryptor = new BasicPasswordEncryptor();
	
	public static String serverIP, serverPORT, masterPORT, guesserPORT, maxgamenum, firstmulticastIP;
	
	public static void main(String[] args) throws IOException{
		Vector<Game> games = new Vector<Game>();
		Vector<Player> players = new Vector<Player>();
		BufferedReader fileInput;
		JSONObject jInput = new JSONObject();
		ServerSocket masterSocket, guesserSocket;
		
		//Creazione di un JSONObject contenente le informazioni di configurazione
		try{
			fileInput = new BufferedReader(new FileReader(configFile));

			jInput = (JSONObject) new JSONParser().parse(fileInput.readLine());
		}
		catch (IOException e){
			System.err.println("Registry couldn't read file : " + configFile);
			System.exit(1);
		}
		catch (ParseException e){
			System.err.println("Registry couldn't parse first line of file : " + configFile);
		}
		
		serverIP = (String) jInput.get("serverIP");
		serverPORT = (String) jInput.get("serverPORT");
		masterPORT = (String) jInput.get("masterPORT");
		guesserPORT = (String) jInput.get("guesserPORT");
		maxgamenum = (String) jInput.get("maxgamenum");
		firstmulticastIP = (String) jInput.get("firstmulticastIP");
		System.out.println("serverIP: "+serverIP+" serverPORT: "+serverPORT
							+"\nmasterPORT: "+masterPORT+" guesserPORT: "+guesserPORT
							+"\nmaxgamenum: "+maxgamenum+" firstmulticastIP: "+firstmulticastIP);
		
		System.setProperty("java.rmi.server.hostname", serverIP);

		Registry reg = LocateRegistry.createRegistry(Integer.parseInt(serverPORT));
		
		IManager manager = new Manager(players, games);
		reg.rebind("Manager", manager);
		
		masterSocket = null;
		guesserSocket = null;
		try {
            masterSocket = new ServerSocket(Integer.parseInt(masterPORT));
            guesserSocket = new ServerSocket(Integer.parseInt(guesserPORT));
        } catch (IOException e) {
            System.err.println("Could not listen on master port or guesser port");
            System.exit(1);
        }
		
		new RegistryMasterThread(masterSocket, players, games).start();
		new RegistryGuesserThread(guesserSocket, players, games).start();
		
		BufferedReader userIn = new BufferedReader(new InputStreamReader(System.in));
		while(!userIn.readLine().equals("exit"));
		System.exit(0);//si fanno terminare tutti i thread		
	}

}

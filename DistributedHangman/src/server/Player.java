package server;

import client.IClientStub;

public class Player {
	protected String nickname;
	protected String password;
	protected boolean logged;
	protected GuesserGameManager guesserThread;
	protected IClientStub callback;
	
	//indica se il giocatore è libero o se è impegnato in una partita o sta aspettando che una
	//partita a cui si è registrato abbia inizio. 
	protected boolean free;
	
	public Player(String nickname){
		this.nickname = nickname;
		this.logged = false;
		this.free = true;
		this.callback = null;
	}

	public Player(String nickname, String password){
		this.nickname = new String(nickname);
		this.password = new String(password);
		this.logged = false;
		this.free = true;
		this.callback = null;
	}
	
	public Player(String nickname, String password, IClientStub callback){
		this.nickname = new String(nickname);
		this.password = new String(password);
		this.logged = false;
		this.free = true;
		this.callback = callback;
	}
	
	public synchronized String getNickName(){
		return new String(this.nickname);
	}
	
	public synchronized String getPassword(){
		return new String(this.password);
	}
	
	public synchronized boolean getLogged(){
		return this.logged;
	}
	
	public synchronized void setLogged(boolean logged){
		this.logged = logged;
	}

	public synchronized boolean getFree(){
		return this.free;
	}
	
	public synchronized void setFree(boolean free){
		this.free = free;
	}
	
	public synchronized GuesserGameManager getGuesserGameManager(){
		return this.guesserThread;
	}
	
	public synchronized void setGuesserGameManager(GuesserGameManager guesserThread){
		this.guesserThread = guesserThread;
	}
	
	public synchronized IClientStub getCallback(){
		return this.callback;
	}
	
	public synchronized void setCallback(IClientStub callback){
		this.callback = callback;
	}
	
	public synchronized boolean equals(Object p){
		Player p1 = (Player) p;
		return this.nickname.equals(p1.getNickName());
	}
	
	public synchronized String toString(){
		return "nickname: " + this.nickname + ", password: " + this.password + ", logged: " + this.logged;
	}
}

package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RegistryGuesserThread extends Thread{
	private ServerSocket socket;
	private Executor threadPool;
	private Vector<Player> players;
	private Vector<Game> games;
	
	public RegistryGuesserThread(ServerSocket socket, Vector<Player> players, Vector<Game> games){
		this.socket = socket;
		this.threadPool = Executors.newCachedThreadPool();
		this.players = players;
		this.games = games;
	}
	
	public void run(){
		boolean exit = false;
		while(!exit){
			try{
				this.threadPool.execute(new GuesserGameManager(this.socket.accept(), this.players, this.games));
			}
			catch (IOException e){
				System.out.println("RegistryGuesserThread: Problems during accept");
				System.exit(1);
			}
		}
	}
}

package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import client.IClientStub;

public interface IManager extends Remote{
	public boolean justRegistered(String nickname) throws RemoteException;
	public void registerClient(String nickname, String password) throws RemoteException;
	public boolean login(String nickname, String password, IClientStub callback) throws RemoteException;
	public void logout(String nickname, String password) throws RemoteException;
}

package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

import client.IClientStub;


public class Manager extends UnicastRemoteObject implements IManager{
	private static final long serialVersionUID = 1L;
	private Vector<Game> games;
	private Vector<Player> players;
	
	public Manager(Vector<Player> players, Vector<Game> games)
			throws RemoteException{
		this.games = games;
		this.players = players;
	}

	public synchronized boolean justRegistered(String nickname) throws RemoteException {
		//per verificare se il giocatore è presente non è importante la password
		return this.players.contains(new Player(nickname));
	}
	
	public synchronized void registerClient(String nickname, String password) throws RemoteException{
		if(!this.justRegistered(nickname)){
			String encryptedPassword = HangmanRegistry.pwdEncryptor.encryptPassword(password);
			Player p = new Player(nickname, encryptedPassword);
			this.players.add(p);
		}		
	}
	
	public boolean login(String nickname, String password, IClientStub callback) throws RemoteException {
		Player p;
		boolean logged = false;
		int index = this.players.indexOf(new Player(nickname));
		
		if(index > -1){
			p = this.players.get(index);
			//si effettua il login se e solo se la password è corretta e
			//non era già stato effettuato il login
			if(HangmanRegistry.pwdEncryptor.checkPassword(password, p.getPassword()) && !p.getLogged()){
				p.setLogged(true);
				p.setCallback(callback);
				logged = true;
				p.getCallback().refreshGamesList(this.gamesList());
			}
		}

		return logged;
	}

	public void logout(String nickname, String password) throws RemoteException {
		Player p;
		int index = this.players.indexOf(new Player(nickname));
		
		if(index > -1){
			p = this.players.get(index);
			if(HangmanRegistry.pwdEncryptor.checkPassword(password, p.getPassword()) && p.getLogged()){
				p.setLogged(false);
				p.setCallback(null);
			}
		}
	}
		
	private String gamesList(){
		String s = "";
		for(Game game : games){
			s += game + "\n";
		}
		
		return s;
	}
}

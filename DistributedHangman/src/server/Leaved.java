package server;

public class Leaved {
	private boolean leaved;
	
	public Leaved(boolean b){
		this.leaved = b;
	}
	
	public boolean getLeaved(){
		return this.leaved;
	}
	
	public void setLeaved(boolean b){
		this.leaved = b;
	}
}

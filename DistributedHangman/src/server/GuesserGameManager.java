package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Vector;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import client.IClientStub;


public class GuesserGameManager extends Thread{
	private static final String multicastPORT = "40000";
	
	private Vector<Player> players;
	private Vector<Game> games;
	private Socket socket;
	
	public GuesserGameManager(Socket socket, Vector<Player> players, Vector<Game> games){
		this.socket = socket;
		this.players = players;
		this.games = games;
	}
	
	@SuppressWarnings("unchecked")
	public void run(){
		BufferedReader input = null;
		PrintWriter output = null;
		JSONObject j = null;
		String nickname, password, gameName;
		Player p = null;
		Game g = null;
		String multicastIP;

		try{
			input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			output = new PrintWriter(this.socket.getOutputStream(), true);
		}
		catch(IOException e){
			System.err.println("GuesserGameManager: Problems with socket's streams");
			System.exit(1);
		}
		
		try {
			j = (JSONObject) new JSONParser().parse(input.readLine());
		} catch (ParseException e1) {
			System.err.println("GuesserGameManager: Problems parsing input");
			System.exit(1);
		} catch (IOException e1) {
			System.err.println("GuesserGameManager: Problems reading input");
			System.exit(1);
		}
		
		nickname = (String) j.get("nickname");
		password = (String) j.get("password");
		gameName = (String) j.get("gameName");
		
		int index = this.games.indexOf(new Game(gameName));
		
		if((p = this.autenticate(nickname, password)) != null && index != -1){

			g = this.games.get(index);
			
			synchronized(g.getMasterMonitor()){
				if(g.getGuessersNUM() < g.getMaxGuessersNUM()){
					g.addGuesser(p);
					p.setFree(false);
					
					this.refreshGameList();
													
					//una volta aggiunto il nuovo giocatore si deve controllare se è stato raggiunto il limite
					//massimo di guesser e in tal caso segnalrlo al master e agli altri guessers
				
					Leaved leaved = new Leaved(false);
					GuesserMonitorThread th = new GuesserMonitorThread(g, input, nickname, leaved);
					th.start();
					
					if(g.getGuessersNUM() == g.getMaxGuessersNUM()){
						g.getMasterMonitor().notifyAll();
					}
									
					while(!leaved.getLeaved() && !g.getAborted() && g.getGuessersNUM() < g.getMaxGuessersNUM()){
						try{
							g.getMasterMonitor().wait();
						}
						catch(InterruptedException e){
							System.err.println("GuesserGameManager: Problems waiting for other guessers");
							System.exit(1);
						}
					}
					
					j = new JSONObject();
					if(!g.getAborted() && !leaved.getLeaved()){
						multicastIP = g.getMulticastIP();						
						j.put("multicastIP", multicastIP);
						j.put("multicastPORT", multicastPORT);
						j.put("aborted", "false");						
						j.put("encryptKey", g.getEncryptKey());
					}
					else{
						j.put("aborted", "true");
						this.refreshGameList();
					}						
				}
			}			
		}
		else{
			System.out.println("Connection rejected");
			j.put("aborted", "true");
		}
		
		output.println(j.toJSONString());
		
		try{
			this.socket.close();
		}
		catch (IOException e){
			System.err.println("GuesserGameManager: Problems closing the socket");
		}
	}
	
	private Player autenticate(String nickname, String password){
		Player p = null;
		int index = this.players.indexOf(new Player(nickname));
		
		if(index > -1){
			p = this.players.get(index);
			if(!(HangmanRegistry.pwdEncryptor.checkPassword(password, p.getPassword())
					&& p.getLogged())){
				p = null;
			}
		}
		
		return p;
	}
	
	private void refreshGameList(){
		String gameList = "";
		for(Game game : games){
			gameList += game + "\n";
		}
		
		try{	
			IClientStub cb = null;
			for(Player p1 : this.players){
				if((cb = p1.getCallback()) != null){
					cb.refreshGamesList(gameList);
				}
			}
		} catch (RemoteException e){
			System.err.println(
					"GuesserGameManager: Problems notifing game list changes to other players");
			System.exit(1);
		}
	}
	
}

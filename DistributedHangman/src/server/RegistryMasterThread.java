package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RegistryMasterThread extends Thread{
	private ServerSocket socket;
	private Executor threadPool;
	private Vector<Player> players;
	private Vector<Game> games;

	public RegistryMasterThread(ServerSocket socket, Vector<Player> players, Vector<Game> games){
		this.socket = socket;
		this.threadPool = Executors.newCachedThreadPool();
		this.players = players;
		this.games = games;
	}
	
	public void run(){
		int MAXGAMENUM = Integer.parseInt(HangmanRegistry.maxgamenum);
		MasterGameManager m;
		IPRange multicastIPs = new IPRange(HangmanRegistry.firstmulticastIP, MAXGAMENUM);
		
		
		while(true){
			if(multicastIPs.isFree()){
				try{
					m = new MasterGameManager(this.socket.accept(), this.players, this.games, multicastIPs);
					this.threadPool.execute(m);
				}
				catch (IOException e){
					System.out.println("RegistryMasterThread: Problems during accept");
					System.exit(1);
				}
			}
		}
	}
}

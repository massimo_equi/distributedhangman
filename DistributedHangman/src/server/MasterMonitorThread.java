package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.SocketException;

public class MasterMonitorThread extends Thread{
	private Game game;
	private BufferedReader input;
	
	public MasterMonitorThread(Game game, BufferedReader input){
		this.game = game;
		this.input = input;
	}
	
	public void run(){
		try{
			while(!input.readLine().equals("exit"));
			synchronized(this.game.getMasterMonitor()){
				this.game.setAborted(true);
				this.game.getMasterMonitor().notifyAll();
			}
		}
		catch(SocketException e){} 
		catch (IOException e) {
			System.err.println("MasterMonitorThread: Problems reading from socket");
		}
	}
}

package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.SocketException;

public class GuesserMonitorThread extends Thread{
	private Game game;
	private BufferedReader input;
	private String nickname;
	private Leaved leaved;
	
	public GuesserMonitorThread(Game game, BufferedReader input, String nickname, Leaved leaved){
		this.game = game;
		this.input = input;
		this.nickname = nickname;
		this.leaved = leaved;
	}
	
	public void run(){
		try{
			while(!input.readLine().equals("exit"));
			synchronized(this.game.getMasterMonitor()){
				this.game.removeGuesser(nickname);
				this.leaved.setLeaved(true);
				this.game.getMasterMonitor().notifyAll();
			}
		}
		catch(SocketException e){} 
		catch (IOException e) {
			System.err.println("GuesserMonitorThread: Problems reading from socket");
		}
	}
}

package server;

import java.util.Vector;


public class Game {
	private final Object masterMonitor = new Object();//oggetto necessario per la sincronizzazione
	
	private String encryptKey;
	private Vector<Player> guessers;	
	private String masterName;
	private int maxGuessers;
	private String multicastIP;
	private Boolean aborted;
	
	public Game(String masterName){
		this.masterName = masterName;
		this.guessers = new Vector<Player>();
		this.aborted = new Boolean(false);
	}
	
	public Game(String masterName, int maxGuessers, String multicastIP){
		this.masterName = masterName;
		this.maxGuessers = maxGuessers;
		this.guessers = new Vector<Player>();
		this.multicastIP = multicastIP;
		this.aborted = new Boolean(false);
	}
	
	public void addGuesser(Player p){
		this.guessers.add(p);
	}
	
	public void removeGuesser(String nickname){
		this.guessers.remove(new Player(nickname));
	}
	
	public int getGuessersNUM(){
		return this.guessers.size();
	}
	
	public int getMaxGuessersNUM(){
		return this.maxGuessers;
	}
	
	public String getMasterName(){
		return new String(this.masterName);
	}
	
	public Object getMasterMonitor(){
		return this.masterMonitor;
	}
		
	public String getMulticastIP(){
		return this.multicastIP;
	}
	
	public boolean getAborted(){
		return this.aborted.booleanValue();
	}
	
	public void setAborted(boolean b){
		this.aborted = new Boolean(b);
	}
	
	public String getEncryptKey(){
		return this.encryptKey;
	}
	
	public void setEcryptKey(String key){
		this.encryptKey = key;
	}
	
	public synchronized boolean equals(Object g){
		Game g1 = (Game) g;
		return this.masterName.equals(g1.getMasterName());
	}
	
	public String toString(){
		return this.masterName+"\t"+this.guessers.size()+"\t"+this.maxGuessers;
	}
}

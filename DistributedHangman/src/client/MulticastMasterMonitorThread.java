package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


public class MulticastMasterMonitorThread extends Thread{
	private BufferedReader input; 
	private DatagramSocket socket;
	private InetAddress groupAddress;
	private String localPORT;
	
	public MulticastMasterMonitorThread(DatagramSocket socket, BufferedReader input,
			InetAddress groupAddress, String localPORT){
		this.input = input;
		this.socket = socket;
		this.groupAddress = groupAddress;
		this.localPORT = localPORT;
	}
	
	public void run(){
		try{
			while(!Thread.currentThread().isInterrupted()){
				if(input.ready())
					if(input.readLine().equals("exitgame")){
						GameMessage newMsg = new GameMessage();
						newMsg.setSenderType("m");
						newMsg.setWinner("g");
						byte[] buf = newMsg.toString().getBytes();
						DatagramPacket packet = new DatagramPacket(buf, buf.length, groupAddress, Integer.parseInt(localPORT));
						socket.send(packet);
						this.socket.close();
					}
				Thread.sleep(300);
			}
		}
		catch(SocketException e){}
		catch(InterruptedException e){}
		catch(IOException e){
			System.err.println("MulticastMasterMonitorThread: Problems with user input");
			System.exit(1);
		}
	}
}

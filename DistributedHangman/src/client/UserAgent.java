package client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import server.IManager;

public class UserAgent {	
	//Codifica del ruolo
	private static final int MASTER = 0;
	private static final int GUESSER = 1;
	
	//File di configurazione
	private static String configFile = "UserAgentConfiguration.json";
	
	//Buffer per la gestione dell'input da tastiera
	private static String userInput;
	
	//variabili usate per la configurazione della comunicazione in multicast
	private static String localIP, localPORT, serverIP, serverPORT, masterPORT, guesserPORT;
	
	public static BasicTextEncryptor stringEncryptor = new BasicTextEncryptor();
				
	public static void main(String[] args){
		int role = -1;
		BufferedReader fileInput;
		JSONObject jInput = new JSONObject();
		IManager mng = null;
		
		//Settaggio del file di configurazione
		if(args.length > 0)
			configFile = "UserAgentConfiguration"+args[0]+".json";
				
		//Creazione di un JSONObject contenente le informazioni di configurazione
		try{
			fileInput = new BufferedReader(new FileReader(configFile));

			jInput = (JSONObject) new JSONParser().parse(fileInput.readLine());
		}
		catch (IOException e){
			System.err.println("UserAgent couldn't read file : " + configFile);
			System.exit(1);
		}
		catch (ParseException e){
			System.err.println("UserAgent couldn't parse first line of file : " + configFile);
		}
		
		localIP = (String) jInput.get("localIP");
		localPORT = (String) jInput.get("localPORT");
		serverIP = (String) jInput.get("serverIP");
		serverPORT = (String) jInput.get("serverPORT");
		masterPORT = (String) jInput.get("masterPORT");
		guesserPORT = (String) jInput.get("guesserPORT");
		
		//Richiesta all'utente di username e password
		try{
			mng = (IManager) Naming.lookup("rmi://" + serverIP + ":" + serverPORT +"/Manager");
		}
		catch (RemoteException e){
			System.err.println("UserAgent: Problems connecting to server via RMI");
			System.exit(1);
		}
		catch (MalformedURLException e){
			System.err.println("UserAgent: server URL not correct");
			System.exit(1);
		}
		catch (NotBoundException e){
			System.err.println("UserAgent: doesn't exist any bound for class Manager");
			System.exit(1);
		}
		
		boolean logged = false;
		String nickname, password;	
		System.out.println("Type your nickname");
		nickname = getUserInput();
		
		System.out.println("Type your password");
		password = getUserInput();	
		System.out.println("\nYour data:\nNickname: "+nickname+" Password: "+password+"\n");
		
		ClientStub cs = null;
		//Procedura di login
		try{
			cs = new ClientStub();
			if(!mng.justRegistered(nickname)){//utente non registrato
				System.out.println("You're not registered. Do you want to sing up now?(y/n)");
				userInput = getUserInput();
				if(userInput.equals("y")){//registrazione e login
					mng.registerClient(nickname, password);
					mng.login(nickname, password, cs);
					System.out.println("Registered and Logged\nNickname: "+nickname+" Password: "+password+"\n");
					logged = true;
				}
				else
					System.out.println("Not Registered");
			}
			else{//utente registrato; si prosegue con il login
				while(!mng.login(nickname, password, cs)){
					System.out.println("Wrong password or user just logged, try again");
					password = getUserInput();
				}
				System.out.println("Logged\n");
				logged = true;
			}
		}
		catch(RemoteException e){
			System.err.println("User Agent: Problems during login or registration");
			System.exit(1);
		}
		
		if(logged){//login effettuato			
			System.out.println("Choose your role: Master or Guesser?(m/g)");
			userInput = getUserInput();
			
			if(userInput.equals("m"))
				role = MASTER;
						
			if(userInput.equals("g"))
				role = GUESSER;
			
			try{
				switch(role){
					case MASTER : {
						masterManager(nickname, password);
						break;					
					}
					case GUESSER : {
						guesserManager(nickname, password);
						break;					
					}
					default : System.out.println("Unknown command");
				}
			}
			catch (UnknownHostException e){
				System.err.println("Preparing for a play: problems with host name");
				System.exit(1);		
			}
			catch (IOException e){
				System.err.println("Preparing for a play: unable to open the socket");
				e.printStackTrace();
				System.exit(1);
			}

			try {
				mng.logout(nickname, password);
			} catch (RemoteException e) {
				System.err.println("UserAgent: Problems durign logout");
				System.exit(1);
			};
		}

		
		try {
			ClientStub.unexportObject(cs, true);
		} catch (NoSuchObjectException e) {
			System.err.println("UserAgent: no client stub object to delete");
			System.exit(1);
		}
		
		System.out.println("Game finished");
		System.exit(0);//si fanno terminare anche i thread sospesi per la ricezione di input da tastiera
		
	}
	
	@SuppressWarnings("unchecked")
	private static void masterManager(String nickname, String password)
			throws IOException, UnknownHostException{
		MasterLogic m = new MasterLogic(nickname, password);		
		String userIn;
		BufferedReader input = null;
		PrintWriter output = null;
		Socket socketTCP = null;
		JSONObject j = null;
		
		//Richiesta delle informazioni per creare la partita (numero di guessers e target del master)
		System.out.println("Yuo're a Master.\nHow many guessers should join this game?");
		userIn = getUserInput();
		
		j = new JSONObject();
		j.put("nickname", nickname);
		j.put("password", password);
		j.put("guessersNUM", userIn);
		
		System.out.println("Type your target");
		userIn = getUserInput();
		m.setTarget(userIn);
		
		//Inizializzazione della socket TCP utilizzata per creare la partita
		try{
			socketTCP = new Socket(serverIP, Integer.parseInt(masterPORT));
			input = new BufferedReader(new InputStreamReader(socketTCP.getInputStream()));
			output = new PrintWriter(socketTCP.getOutputStream(), true);
		}
		catch(IOException e){
			System.out.println("User Agent: Problems configuring the TCP scoket");
			System.exit(1);
		}
		
		//Comunicazione alla Registry dei dati necessari per creare la partita
		output.println(j.toJSONString());		
		System.out.println("Waiting for guessers...");
		
		BufferedReader thInput = new BufferedReader(new InputStreamReader(System.in));
		TCPMonitorThread th = 
				new TCPMonitorThread(output, thInput);
		th.start();
		//Attesa della risposta della Registry contenente l'indirizzo di multicast per la partita
		try {
			j = (JSONObject) new JSONParser().parse(input.readLine());
		} catch (ParseException e) {
			System.err.println("UserAgent: Problems parsing TCP socket input");
			System.exit(1);
		}
		catch (IOException e) {
			System.err.println("UserAgent: Problems reading TCP scoket input");
			System.exit(1);
		}
		th.interrupt();
		socketTCP.close();
		
		String aborted = (String) j.get("aborted");
		if(aborted.equals("true"))
			return;
		
		String groupIP = (String) j.get("multicastIP");
		int groupPORT = Integer.parseInt((String) j.get("multicastPORT"));
		String encryptKey = (String) j.get("encryptKey");
		System.out.println("encryption key: "+ encryptKey);
		
		//Inizializzazione della socket per la comunicazione multicast durante la partita

		//acquisizione dell'indirizzo multicast
        InetAddress groupAddress = InetAddress.getByName(groupIP);
        
		//apertura del socket multicast
		MulticastSocket socket = joinMulticast(groupAddress, groupPORT);

		System.out.println("Your target is: " + m.getTarget()+"\nWait guessers' attempts...");
		
		//Inizio del ciclo di invio del feedback e ricezione dei guesses
		boolean exit = false;
		DatagramPacket packet;
		String received;
		byte[] buf;
		int timeout = 600000/*10 minuti*/, elapsed = 0;
		long before = 0, after = 0;
		UserAgent.stringEncryptor.setPassword(encryptKey);
				
		MulticastMasterMonitorThread thMul = new MulticastMasterMonitorThread(socket, thInput, groupAddress, localPORT);
		thMul.start();
		while(!exit && timeout > elapsed){
			//Si aspettano risposte dai guessers
			socket.setSoTimeout(timeout - elapsed);
			before = System.currentTimeMillis();
			received = null;
			try{
				received = receiveMulticast(socket);
				System.out.println("decrypting...");
				received = UserAgent.stringEncryptor.decrypt(received);
				System.out.println("decrypted.");
			}
			catch(SocketException e){
				exit = true;
			} catch (EncryptionOperationNotPossibleException e){
				received = null;
			}
            
            after = System.currentTimeMillis();
            elapsed = (int)(after - before);
            
            if(received != null){
                GameMessage recMsg = null;
	            try{
	            	recMsg = new GameMessage(received);
	            }
	            catch (ParseException e){
	            	System.err.println("Master: Problems parsing received message");
	            	System.exit(1);
	            }
	            
	            //Si aggiorna lo stato della partita in base al nuovo guess
	            if(recMsg.getSenderType().equals("g")){
	            	m.attempt(recMsg.getNewLetter());
		            System.out.println("Actual State\n"+
		            "Target: "+ m.getTarget() +" Guess: "+m.getGuess() +
		            " Remaining attempts: " + m.getAttempts());
	            	            
					//si invia la nuova situazione ai guessers
					GameMessage newMsg = new GameMessage();
					newMsg.setSenderType("m");
					newMsg.setLastSender(recMsg.getNickNameSender());
					newMsg.setLastLetter(recMsg.getNewLetter());
					newMsg.setState(m.getGuess());
					newMsg.setRemAttempts(new Integer(m.getAttempts()).toString());
					
					if(m.getWin()){
		            	exit = true;
		            	newMsg.setWinner("g");
		            	System.out.println("Guessers win");
		            }
		            
		            if(m.getAttempts() == 0){
		            	exit = true;
		            	newMsg.setWinner("m");
		            	System.out.println("Master wins");
		            }
		            
		            System.out.println("encrypting...");
					buf = UserAgent.stringEncryptor.encrypt(newMsg.toString()).getBytes();
					System.out.println("encrypted.");
					packet = new DatagramPacket(buf, buf.length, groupAddress, Integer.parseInt(localPORT));
					socket.send(packet);
				
	            }
            }
		}
		thMul.interrupt();
		socket.close();

	}
	
	@SuppressWarnings("unchecked")
	private static void guesserManager(String nickname, String password)
			throws UnknownHostException, IOException{
		BufferedReader input = null;
		PrintWriter output = null;
		Socket socketTCP = null;
		JSONObject j = null;
		String userIn = null;
		byte[] buf;
		boolean exit = false;
		int TIMEOUT = 10000;
		
		//Richiesta delle informazioni per partecipare ad una partita (nome della partita)
		System.out.println("You're a Guesser\nWhich game do you want to join?");
		userIn = getUserInput();

		j = new JSONObject();
		j.put("nickname", nickname);
		j.put("password", password);
		j.put("gameName", userIn);
		
		//Inizializzazione della socket TCP utilizzata per partecipare alla partita
		try{
			socketTCP = new Socket(serverIP, Integer.parseInt(guesserPORT));
			input = new BufferedReader(new InputStreamReader(socketTCP.getInputStream()));
			output = new PrintWriter(socketTCP.getOutputStream(), true);
		}
		catch(IOException e){
			System.out.println("User Agent: Problems configuring the TCP scoket");
			System.exit(1);
		}
				
		//Comunicazione alla Registry dei dati necessari per partecipare alla partita
		output.println(j.toJSONString());
		
		System.out.println("Waiting for other guessers...");

		BufferedReader thInput = new BufferedReader(new InputStreamReader(System.in));
		TCPMonitorThread th = 
				new TCPMonitorThread(output, thInput);
		th.start();
		
		//Attesa della risposta della Registry contenente l'indirizzo di multicast per la partita
		try {
			j = (JSONObject) new JSONParser().parse(input.readLine());
		} catch (ParseException e) {
			System.err.println("UserAgent: Problems parsing TCP socket input");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("UserAgent: Problems reading TCP scoket input");
			System.exit(1);
		}
		socketTCP.close();
		th.interrupt();
		
		String aborted = (String) j.get("aborted");
		if(aborted.equals("true"))
			return;
		
		String groupIP = (String) j.get("multicastIP");
		int groupPORT = Integer.parseInt((String) j.get("multicastPORT"));
		String encryptKey = (String) j.get("encryptKey");
		System.out.println("encryption key: "+ encryptKey);
		//Acquisizione dell'indirizzo multicast
        InetAddress groupAddress = InetAddress.getByName(groupIP);
        
		//Inizializzazione della socket per la comunicazione		
		MulticastSocket socket = joinMulticast(groupAddress, groupPORT);

		UserAgent.stringEncryptor.setPassword(encryptKey);
		ACK mACK = new ACK();
		MulticastGuesserMonitorThread thMul = new MulticastGuesserMonitorThread(socket, mACK, nickname);
		thMul.start();
		
        System.out.println("Multicast group joined.\nFirst Attempt.");
		System.out.println("Type a letter to send or 'exitgame' to exit");
      
        while(!exit){

			//si legge la lettera da spedire
			userIn = getUserInput();			
			
			if(userIn.equals("exitgame"))
				exit = true;
			else{
				//Si spedisce la lettera
				
				//Preparazione del messaggio
				buf = new byte[256];
				GameMessage newMsg = new GameMessage();
				newMsg.setNewLetter(userIn);
				newMsg.setSenderType("g");
				newMsg.setNickNameSender(nickname);
				
				//Spedizione
				System.out.println("encrypting...");
				buf = UserAgent.stringEncryptor.encrypt(newMsg.toString()).getBytes();
				System.out.println("encrypted.");
				InetAddress group = InetAddress.getByName(groupIP);
				DatagramPacket packet = new DatagramPacket(buf, buf.length, group, Integer.parseInt(localPORT));
				
				//Attesa dell'ACK del Master
				System.out.println("Waiting Master ACK...");
				synchronized(mACK){
					while(!mACK.getACK()){
						socket.send(packet);
						try {
							mACK.wait(TIMEOUT);
						} catch (InterruptedException e) {
							System.err.println("UserAgent: Interrupted during wait()");
							System.exit(1);
						}					
					}
					if(mACK.getWinner().equals("m") || mACK.getWinner().equals("g"))
						exit = true;
					mACK.setACK(false);
				}			
			}			
		}
        
		socket.leaveGroup(InetAddress.getByName(groupIP));
		socket.close();
	}

	private static MulticastSocket joinMulticast(InetAddress groupAddress, int groupPORT) throws IOException,
			SocketException, UnknownHostException {
		//apertura del socket mutlicast
    	MulticastSocket socket = new MulticastSocket(groupPORT);
    	
    	//acquisizione dell'indirizzo multicast
        socket.setInterface(InetAddress.getByName(localIP));
		        
        //registrazione al gruppo multicast
        socket.joinGroup(groupAddress);
		return socket;
	}
	

	private static String getUserInput(){
		Scanner s;		
		byte[] buf = new byte[256];
		try{
			System.in.read(buf);
		}
		catch (IOException e){
			System.err.println("User Agent: Couldn't read from stdin");
			System.exit(1);
		}
		String userInput = new String(buf);
		s = new Scanner(userInput);
		userInput = s.next();
		s.close();
		
		return userInput;
	}
	
	private static String receiveMulticast(MulticastSocket socket) throws IOException {
		DatagramPacket packet;
		boolean timeout = false;
		byte[] buf = new byte[256];
        packet = new DatagramPacket(buf, buf.length);
        try{
        	socket.receive(packet);
        }
        catch(SocketTimeoutException e){
        	timeout = true;
        }
        String received = null;
        if(!timeout)
        	received = new String(packet.getData(), 0, packet.getLength());
        
		return received;
	}
}

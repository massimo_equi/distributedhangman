package client;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GameMessage {
	private String senderType = "";
	private String nickNameSender = "";
	private String newLetter = "";
	private String lastSender = "";
	private String lastLetter = "";
	private String state = "";
	private String remAttempts = "";
	private String winner = "none";
		
	public GameMessage(){	
	}
	
	public GameMessage(String s) throws ParseException{
		JSONObject j = new JSONObject();
		j = (JSONObject) new JSONParser().parse(s);
		
		this.senderType = (String) j.get("senderType");
		this.nickNameSender = (String) j.get("nickNameSender");
		this.newLetter = (String) j.get("newLetter");
		this.lastSender = (String) j.get("lastSender");
		this.lastLetter = (String) j.get("LastLetter");
		this.state = (String) j.get("state");
		this.remAttempts = (String) j.get("remAttempts");
		this.winner = (String) j.get("winner");
	}
		
	@SuppressWarnings("unchecked")
	public JSONObject toJSON(){
		JSONObject j = new JSONObject();
		
		j.put("senderType", this.senderType);
		j.put("nickNameSender", this.nickNameSender);
		j.put("newLetter", this.newLetter);
		j.put("lastSender", this.lastSender);
		j.put("lastLetter", this.lastLetter);
		j.put("state", this.state);
		j.put("remAttempts", this.remAttempts);	
		j.put("winner", this.winner);
		
		return j;
	}
	
	public void setSenderType(String s){
		this.senderType = s;
	}
	
	public void setNickNameSender(String s){
		this.nickNameSender = s;
	}
	
	public void setNewLetter(String s){
		this.newLetter = s;
	}
	
	public void setLastSender(String s){
		this.lastSender = s;
	}
	
	public void setLastLetter(String s){
		this.lastLetter = s;
	}
	
	public void setState(String s){
		this.state = s;
	}
	
	public void setRemAttempts(String s){
		this.remAttempts = s;
	}
	
	public void setWinner(String s){
		this.winner = s;
	}
	
	public String getSenderType(){
		return this.senderType;
	}
	
	public String getNickNameSender(){
		return this.nickNameSender;
	}
	
	public String getNewLetter(){
		return this.newLetter;
	}
	
	public String getLastSender(){
		return this.lastSender;
	}
	
	public String getLastLetter(){
		return this.lastLetter;
	}
	
	public String getState(){
		return this.state;
	}
	
	public String getRemAttemps(){
		return this.remAttempts;
	}
	
	public String getWinner(){
		return this.winner;
	}
	
	@SuppressWarnings("unchecked")
	public String toString(){
		JSONObject j = new JSONObject();
	
		j.put("senderType", this.senderType);
		j.put("nickNameSender", this.nickNameSender);		
		j.put("newLetter", this.newLetter);
		j.put("lastSender", this.lastSender);
		j.put("lastLetter", this.lastLetter);
		j.put("state", this.state);
		j.put("remAttempts", this.remAttempts);		
		j.put("winner", this.winner);		
		return j.toJSONString();
	}
}

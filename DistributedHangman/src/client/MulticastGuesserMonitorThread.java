package client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.SocketException;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.json.simple.parser.ParseException;

public class MulticastGuesserMonitorThread extends Thread{
	private MulticastSocket socket;
	private ACK mACK;
	private String nickname;
	
	public MulticastGuesserMonitorThread(MulticastSocket socket, ACK mACK, String nickname){
		this.socket = socket;
		this.mACK = mACK;
		this.nickname = nickname;
	}
	
	public void run(){		
		System.out.println("Actual state is: ?UNKNOWN WORD?");
		boolean exit = false, error = false;
		while(!exit){
			//Ricezione del pacchetto
			String received = null;
	        try {
				received = receiveMulticast(this.socket);
				System.out.println("decrypting...");
				received = UserAgent.stringEncryptor.decrypt(received);
				System.out.println("decrypted.");
			} catch (SocketException e) {
				exit = true;
			} catch (EncryptionOperationNotPossibleException e){
				error = true;
			} catch (IOException e) {
				System.err.println("MulticastGuesserMonitorThread: Problems waiting for incoming packets");
				System.exit(1);
			}
	        
	        if(!exit && !error){
		        //Parsing del pacchetto
		        GameMessage recMsg = null;
		        try {		        	
					recMsg = new GameMessage(received);
				} catch (ParseException e) {
					System.out.println("MulticastGuesserMonitorThread: Problems parsing received message");
					System.exit(1);
				}
		        
		        //Aggiornamento dello stato
		        if(recMsg != null && recMsg.getSenderType().equals("m")){
		        	if(recMsg.getLastSender().equals(nickname)
		        			|| recMsg.getWinner().equals("m") || recMsg.getWinner().equals("g")){
			        	synchronized(mACK){
			        		this.mACK.setACK(true);
			        		this.mACK.setWinner(recMsg.getWinner());
			        		this.mACK.notify();
			        	}
		        	}					
					System.out.println("Actual state is:  "+ recMsg.getState() +
										" Remaining attempts: "+recMsg.getRemAttemps());
					if(recMsg.getWinner().equals("m"))
						System.out.println("Master wins");
					if(recMsg.getWinner().equals("g"))
						System.out.println("Guessers win");
		        }
		        error = false;	
	        }
		}
	}

	private static String receiveMulticast(MulticastSocket socket) throws IOException {
		DatagramPacket packet;
		byte[] buf = new byte[256];
        packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);
        String received = new String(packet.getData(), 0, packet.getLength());
		return received;
	}
}

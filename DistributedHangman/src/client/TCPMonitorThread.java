package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;

public class TCPMonitorThread extends Thread{
	private PrintWriter output;
	private BufferedReader input;
	
	public TCPMonitorThread(PrintWriter output, BufferedReader input){
		this.output = output;
		this.input = input;
	}
	
	public void run(){
		try{
			while(!Thread.currentThread().isInterrupted()){
				if(input.ready())
					if(input.readLine().equals("exitgame"))
						this.output.println("exit");
				Thread.sleep(300);
			}
		}
		catch(SocketException e){}
		catch(InterruptedException e){}
		catch(IOException e){
			System.err.println("TCPMonitorThread: Problems with user input");
			System.exit(1);
		}
	}	
}

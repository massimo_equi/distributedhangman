package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClientStub extends Remote{
	public void refreshGamesList(String games) throws RemoteException;
}

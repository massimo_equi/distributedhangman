package client;

public class ACK {
	private boolean ack;
	private String winner;
	
	public ACK(){
		this.ack = false;
		this.winner = "none";
	}
	
	public boolean getACK(){
		return this.ack;
	}
	
	public void setACK(boolean b){
		this.ack = b;
	}
	
	public String getWinner(){
		return this.winner;
	}
	
	public void setWinner(String w){
		this.winner = w;
	}
}

package client;

import java.util.LinkedList;

import server.Player;

public class MasterLogic extends Player{
	private static final String voidChar = "?";
	private String target;
	private String guess;
	private LinkedList<String> triedLetters;
	private boolean win;
	private int attempts;
	
	public MasterLogic(String nickname, String password){
		super(nickname, password);
		this.target = null;
		this.guess = null;
		this.attempts = 10;
		this.triedLetters = new LinkedList<String>();
		this.win = false;
	}
	
	public MasterLogic(String nickname, String password, String target){
		super(nickname, password);
		this.attempts = 10;
		this.win = false;
		this.target = new String(target);
		this.guess = "";
		this.triedLetters = new LinkedList<String>();
		for(int i = 0; i < target.length(); i++)
			this.guess += voidChar;
	}
	
	public void setTarget(String s){
		this.target = new String(s);
		this.attempts = 10;
		this.win = false;
		int i;
		this.guess = "";
		for(i = 0; i < s.length(); i++)
			this.guess += voidChar;
	}
	
	public boolean attempt(String c){
		//restituisce 'true' se 'c' è presente in 'this.guess', 'false' altrimenti
		boolean res;
		String s1, s2;
		int i;
		
		//il carattere non è mai stato provato
		if(!this.guess.contains(c)){			
			//si controlla se la lettera è presente
			if(res = this.target.contains(c)){
				//si aggiorna il guess
				for(i = 0; i< this.target.length(); i++){
					if(this.target.substring(i, i+1).equals(c)){
						s1 = this.guess.substring(0, i);
						s2 = this.guess.substring(i+1, this.guess.length());
						this.guess = s1 + c + s2;
					}
				}
			}
			else
				if(!this.triedLetters.contains(c))
					this.attempts--;
		}
		else
			res = true;
		this.triedLetters.add(c);
		if(this.target.equals(this.guess) && this.attempts >= 0)
			this.win = true;
		
		return res;
	}
	
	public String getTarget(){
		return new String(this.target);
		
	}
	
	public String getGuess(){
		return new String(this.guess);
		
	}
	
	public int getAttempts(){
		return this.attempts;
	}
	
	public boolean getWin(){
		return this.win;
	}
	
	public String toString(){
		return "nickname: " + this.nickname + ", target: " + this.target + ", guess: " + this.guess
				+ ", attempts: " + this.attempts + ", win: " + this.win;
	}
	
}
